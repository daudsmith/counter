import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const Counter = () => {
  const [count, setCount] = useState(0);

  const incrementCount = () => {
    setCount(count + 1);
  };

  const decrementCount = () => {
    if (count > 0) {
      setCount(count - 1);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.countText}>Counting: {count}</Text>
      <View style={styles.buttonContainer}>
        <View style={styles.buttonView}>
          <Button style={styles.button} title="-" onPress={decrementCount}/>
        </View>
          <View style={styles.buttonSpacer} />
        <View style={styles.buttonView}>
          <Button style={styles.button} title="+" onPress={incrementCount} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  countText: {
    fontSize: 35,
    marginBottom: 50,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center', // Menggunakan justifyContent untuk menengahkan tombol
    alignItems: 'center', // Menggunakan alignItems untuk menengahkan tombol secara vertikal
  },
  buttonView: {
    width: 80,
  },
  buttonSpacer: {
    width: 20, // Atur lebar spasi sesuai kebutuhan
  },
});

export default Counter;